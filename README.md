## Options:

**Directory** + **Filename**: Path of the commands file.      
**Delay**: Time between each command in ms. This is added to prevent server from kicking you from sending too many commands.       
**Always Reload**: Reload the file each time the macro is used, used if the commands in the file can change. 