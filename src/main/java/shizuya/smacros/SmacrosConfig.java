package shizuya.smacros;

import java.io.File;

import me.shedaniel.autoconfig.annotation.Config;
import me.shedaniel.autoconfig.AutoConfig;
import me.shedaniel.autoconfig.ConfigData;
import net.fabricmc.loader.api.FabricLoader;

@Config(name = "smacros")
public class SmacrosConfig implements ConfigData {
    private String directory = FabricLoader.getInstance().getConfigDir().toString() + File.separator + "smacros";
    private String filename = ".txt";
    private int delay = 1000;
    private boolean alwaysReload = false;

    @Override
    public void validatePostLoad() {
        if (this.delay < 0) {
            this.delay = 0;
        }
    }
    
    public static SmacrosConfig getConfig() {
        return AutoConfig.getConfigHolder(SmacrosConfig.class).get();
    }

    public String getDirectory() {
        return this.directory;
    }

    public String getFilename() {
        return this.filename;
    }

    public int getDelay() {
        return this.delay;
    }

    public boolean getAlwaysReload() {
        return this.alwaysReload;
    }

}
