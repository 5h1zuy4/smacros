package shizuya.smacros;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.util.List;
import java.util.ArrayList;
import java.util.Queue;
import java.util.LinkedList;

import org.lwjgl.glfw.GLFW;
import me.shedaniel.autoconfig.AutoConfig;
import me.shedaniel.autoconfig.serializer.GsonConfigSerializer;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.option.KeyBinding;
import net.minecraft.client.util.InputUtil;
import net.minecraft.text.Text;

public class Smacros implements ClientModInitializer {

    private final KeyBinding USE = KeyBindingHelper.registerKeyBinding(new KeyBinding("key.smacros.use", InputUtil.Type.KEYSYM, GLFW.GLFW_KEY_EQUAL, "smacros"));

    public static MinecraftClient client = null;
    private String fileLoaded = "";
    private List<String> commands = new ArrayList<String>();
    private Queue<String> queue = new LinkedList<String>();
    private int time = 0;
    
    @Override
    public void onInitializeClient() {
        client = MinecraftClient.getInstance();
		AutoConfig.register(SmacrosConfig.class, GsonConfigSerializer::new);
        ClientTickEvents.END_CLIENT_TICK.register(this::onEndClientTick);
    }

    private void onEndClientTick(MinecraftClient client) {
        if (USE.wasPressed()) {
            if (SmacrosConfig.getConfig().getFilename() != fileLoaded || SmacrosConfig.getConfig().getAlwaysReload()) {
                loadMacros();
                fileLoaded = SmacrosConfig.getConfig().getFilename();
            }
            queue.addAll(commands);
        }
        if (time <= 0) {
            String command = queue.poll();
            if (command != null) {
                sendCommand(command);
                time = SmacrosConfig.getConfig().getDelay();
            }
        } else {
            time -= 50;
        }
    }

    private void loadMacros() {
        commands.clear();
        try {
            BufferedReader br = new BufferedReader(new FileReader(SmacrosConfig.getConfig().getDirectory() + File.separator + SmacrosConfig.getConfig().getFilename()));
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                commands.add(line);
            }
            br.close();
        }
        catch (Exception e) {
        }
    }

    private void sendCommand(String command) {
        if (client.player == null) {
            return;
        }
        if (command.startsWith("/")) {
            client.player.networkHandler.sendChatCommand(command.substring(1));
        } else if (command.startsWith("#")) {
            client.player.sendMessage(Text.literal(command.substring(1)));
        } else {
            client.player.networkHandler.sendChatMessage(command);
        }
    }
}
